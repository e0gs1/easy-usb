mod bindings {
    windows::include_bindings!();
}
use std::mem::size_of;

use crate::error;
use bindings::{Windows::Win32::Devices::PortableDevices::*, Windows::Win32::Foundation::PWSTR};
use windows::{create_instance, initialize_sta};

pub fn pwstr_to_string(text: &[u16]) -> String {
    String::from_utf16_lossy(
        &text
            .iter()
            .take_while(|b| **b != 0u16)
            .cloned()
            .collect::<Vec<_>>(),
    )
}

pub fn get_friendly_name(
    ipdm: &IPortableDeviceManager,
    device: &PWSTR,
) -> error::Result<Option<String>> {
    let mut name_count: u32 = 0;
    let hr = unsafe { ipdm.GetDeviceFriendlyName(device, None, &mut name_count) };
    if hr.is_err() {
        return Err(error::Error::WindowsError(hr.into()));
    }

    if name_count > 0 {
        let mut text = vec![0u16; name_count as usize];
        let hr = unsafe {
            ipdm.GetDeviceFriendlyName(device, PWSTR(text.as_mut_ptr()), &mut name_count)
        };
        if hr.is_err() {
            return Err(error::Error::WindowsError(hr.into()));
        }
        return Ok(Some(pwstr_to_string(&text)));
    }
    Ok(None)
}

pub fn get_description(
    ipdm: &IPortableDeviceManager,
    device: &PWSTR,
) -> error::Result<Option<String>> {
    let mut description_count: u32 = 0;
    let hr = unsafe { ipdm.GetDeviceDescription(device, None, &mut description_count) };
    if hr.is_err() {
        return Err(error::Error::WindowsError(hr.into()));
    }
    if description_count > 0 {
        let mut text = vec![0u16; description_count as usize];
        let hr = unsafe {
            ipdm.GetDeviceDescription(device, PWSTR(text.as_mut_ptr()), &mut description_count)
        };
        if hr.is_err() {
            return Err(error::Error::WindowsError(hr.into()));
        }
        return Ok(Some(pwstr_to_string(&text)));
    }
    Ok(None)
}

pub fn get_manufacturer(
    ipdm: &IPortableDeviceManager,
    device: &PWSTR,
) -> error::Result<Option<String>> {
    let mut manufacturer_count: u32 = 0;
    let hr = unsafe { ipdm.GetDeviceManufacturer(device, None, &mut manufacturer_count) };
    if hr.is_err() {
        return Err(error::Error::WindowsError(hr.into()));
    }
    if manufacturer_count > 0 {
        let mut text = vec![0u16; manufacturer_count as usize];
        let hr = unsafe {
            ipdm.GetDeviceManufacturer(device, PWSTR(text.as_mut_ptr()), &mut manufacturer_count)
        };
        if hr.is_err() {
            return Err(error::Error::WindowsError(hr.into()));
        }
        return Ok(Some(pwstr_to_string(&text)));
    }
    Ok(None)
}

#[derive(Debug)]
pub struct WPortableDevice {
    pub instance: IPortableDevice,
    pub id: PWSTR,
    pub name: Option<String>,
    pub description: Option<String>,
    pub manufacturer: Option<String>,
}

impl WPortableDevice {
    fn new(
        id: PWSTR,
        name: Option<String>,
        description: Option<String>,
        manufacturer: Option<String>,
    ) -> error::Result<Self> {
        let instance: Result<IPortableDevice, windows::Error> = create_instance(&PortableDevice);
        let instance = instance.unwrap();

        Ok(WPortableDevice {
            instance,
            id,
            name,
            description,
            manufacturer,
        })
    }
}

impl crate::IPortableDevice for WPortableDevice {
    fn name(&self) -> Option<&String> {
        self.name.as_ref()
    }

    fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }

    fn manufacturer(&self) -> Option<&String> {
        self.manufacturer.as_ref()
    }
}

#[derive(Debug, Default)]
pub struct WPortableDevices {
    pub devices: Vec<WPortableDevice>,
}

impl<'a> crate::IPortableDeviceManager<'a> for WPortableDevices {
    fn devices(&'a self) -> Vec<&'a dyn crate::IPortableDevice> {
        self.devices
            .iter()
            .map(|d| d as &dyn crate::IPortableDevice)
            .collect()
    }
}

impl WPortableDevices {
    pub fn new() -> error::Result<Self> {
        initialize_sta()?;

        let pdm: Result<IPortableDeviceManager, windows::Error> =
            create_instance(&PortableDeviceManager);
        if let Ok(pdm) = pdm {
            let mut portable_devices = WPortableDevices::default();

            let device_ids = unsafe {
                let mut device_count = 0;
                let hr = pdm.GetDevices(PWSTR::NULL.0 as *mut PWSTR, &mut device_count);
                if hr.is_err() {
                    return Err(error::Error::WindowsError(hr.into()));
                }
                let mut device_ids: Vec<PWSTR> =
                    vec![PWSTR([0; size_of::<PWSTR>()].as_mut_ptr()); device_count as usize];
                let hr = pdm.GetDevices(device_ids.as_mut_ptr() as *mut PWSTR, &mut device_count);
                if hr.is_err() {
                    return Err(error::Error::WindowsError(hr.into()));
                }
                device_ids
            };

            for device in device_ids.iter() {
                let portable_device = WPortableDevice::new(
                    *device,
                    get_friendly_name(&pdm, device)?,
                    get_description(&pdm, device)?,
                    get_manufacturer(&pdm, device)?,
                );

                portable_devices.devices.push(portable_device?);
            }

            Ok(portable_devices)
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Failed to create IPortableDeviceManager instance",
            )
            .into())
        }
    }
}
