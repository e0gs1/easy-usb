pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    WindowsError(windows::Error),
    IOError(std::io::Error),
}

impl From<windows::Error> for Error {
    fn from(err: windows::Error) -> Self {
        Error::WindowsError(err)
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::IOError(err)
    }
}
