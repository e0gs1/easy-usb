mod error;

#[cfg(target_os = "windows")]
mod win;
#[cfg(target_os = "windows")]
pub use win::*;

#[cfg(target_os = "windows")]
pub struct PortableDeviceManager(win::WPortableDevices);

impl PortableDeviceManager {
    pub fn new() -> error::Result<Self> {
        Ok(PortableDeviceManager(win::WPortableDevices::new()?))
    }
}

impl std::ops::Deref for PortableDeviceManager {
    type Target = win::WPortableDevices;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for PortableDeviceManager {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub trait IPortableDeviceManager<'a>: std::fmt::Debug {
    fn devices(&'a self) -> Vec<&'a dyn IPortableDevice>;
}

pub trait IPortableDevice: std::fmt::Debug {
    fn name(&self) -> Option<&String>;
    fn description(&self) -> Option<&String>;
    fn manufacturer(&self) -> Option<&String>;
}
